import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  url: string = 'http://allepalmeira.com.br/ti04-kibeleza/admin/';
  // url: string = 'localhost:8080/ti04-kibeleza/admin/';

  constructor() {}

  pegarUrl() {
    return this.url;
  }
}
